<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\SyncTMProducts;
class AdminController extends AbstractController
{
    /**
     * @Route("/admin321", name="admin")
     */
    public function index()
    {
        $parser = new SyncTMProducts();
        $products=$parser->getProducts();
        
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController', 
            'products'=>$products
        ]);
    }
}
