<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191201150456 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, category INT NOT NULL, date_parsed DATETIME NOT NULL, params JSON DEFAULT NULL, sizes VARCHAR(255) DEFAULT NULL, images LONGTEXT DEFAULT NULL, model VARCHAR(255) DEFAULT NULL, images_timodna LONGTEXT DEFAULT NULL, status SMALLINT NOT NULL, articul VARCHAR(255) DEFAULT NULL, keywords LONGTEXT DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, is_new SMALLINT DEFAULT NULL, has_discount TINYINT(1) DEFAULT NULL, discount DOUBLE PRECISION DEFAULT NULL, sale TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product');
    }
}
